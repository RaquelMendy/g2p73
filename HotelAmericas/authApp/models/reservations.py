from django.db import models
from .users import User
from .rooms import Room
class Reserva(models.Model):
 id_reserva = models.BigAutoField(primary_key=True)
 user = models.ForeignKey(User, related_name='reservations', on_delete=models.CASCADE)
 id_room = models.IntegerField(default=0)
 cantidadAcompañantes = models.IntegerField(default=0)
 chekin = models.DateTimeField()
 chekout = models.DateTimeField()
