from django.db import models
from .services import Servicio
from .reservations import Reserva
class ReservaHasServicio(models.Model):
 reservaId = models.ForeignKey(Reserva, related_name='reserv_services', on_delete=models.CASCADE)
 servicioId = models.ForeignKey(Servicio, related_name='reserv_services', on_delete=models.CASCADE)
 
 