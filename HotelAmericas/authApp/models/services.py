from django.db import models

class Servicio(models.Model):
 id_servicio = models.AutoField(primary_key=True)
 nombreServicio= models.CharField('Name', max_length = 100)
 precioServicio = models.IntegerField(default=0)

