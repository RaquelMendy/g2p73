from django.db import models

class Room(models.Model):
 id_room = models.IntegerField(primary_key=True)
 descripcion= models.CharField('Name', max_length = 300)
 precio = models.IntegerField(default=0)
 disponibilidad = models.BooleanField()
 
