from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from authApp.models.rooms import Room
from authApp.serializers.roomsSerializer import RoomSerializer


class RoomModifyView(generics.UpdateAPIView):
   queryset = Room.objects.all()
   serializer_class = RoomSerializer
   lookup_field = 'pk'

   def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({"message": "El registro se modificó exitosamente"})

        else:
            return Response({"message": "failed", "details": serializer.errors})