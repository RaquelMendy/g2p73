from rest_framework import serializers
from authApp.models.users import User
from authApp.models.reservations import Reserva
from authApp.serializers.reservationsSerializer import ReservaSerializer
from authApp.serializers.roomsSerializer import RoomSerializer

class UserSerializer(serializers.ModelSerializer):
    reserva = ReservaSerializer()
    class Meta:
        model = User
        fields = ['id','password','name','lastName','fechaNacimiento','email','telefono', 'promocionMensaje', 'promocionCorreo', 'username','reserva']

    def create(self, validated_data):
        reservaData = validated_data.pop('reserva')
        userInstance = User.objects.create(**validated_data)
        Reserva.objects.create(user=userInstance, **reservaData)
        return userInstance

    def to_representation(self, obj):
        try:
            user = User.objects.get(id=obj.id)
            reserva = Reserva.objects.get(user=obj.id)
            return {
                    'id': user.id,
                    'username': user.username,
                    'name': user.name,
                    'lastName': user.lastName,
                    'email': user.email,
                    'telefono': user.telefono,
                    'reserva': {
                        'id': reserva.id_reserva,
                        'No. Acompañantes': reserva.cantidadAcompañantes,
                        'chekin':reserva.chekin,
                        'chekout':reserva.chekout,
                        'Habitación': reserva.id_room,
                      }
            }
        except:
            user = User.objects.get(id=obj.id)
            return {
                    'id': user.id,
                    'username': user.username,
                    'name': user.name,
                    'lastName': user.lastName,
                    'email': user.email,
                    'telefono': user.telefono,
                    'reserva': "sin Reserva"
                     }