from authApp.models.rooms import Room
from rest_framework import serializers

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['id_room', 'descripcion', 'precio', 'disponibilidad']