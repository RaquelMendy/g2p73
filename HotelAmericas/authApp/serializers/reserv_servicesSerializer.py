from authApp.models.reserv_services import ReservaHasServicio
from rest_framework import serializers
class ReservaHasServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReservaHasServicio
        fields = ['reservaId', 'servicioId']
