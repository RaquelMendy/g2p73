from authApp.models.reservations import Reserva
from authApp.models.users import User
from rest_framework import serializers
class ReservaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['id_room','cantidadAcompañantes', 'chekin', 'chekout']
