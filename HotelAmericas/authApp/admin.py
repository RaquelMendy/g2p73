from django.contrib import admin
from .models.reservations import Reserva
from .models.users import User
from .models.rooms import Room
from .models.services import Servicio
from .models.reserv_services import ReservaHasServicio
# Register your models here.
admin.site.register(Servicio)
admin.site.register(Reserva)
admin.site.register(User)
admin.site.register(Room)
admin.site.register(ReservaHasServicio)